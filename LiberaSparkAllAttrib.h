//=============================================================================
//
// file :        IOAttrib.h
//
// description : Include for the NIDaq attribute class.
//
// project :     DAQ Abstract Classes
//
// $Author: epaud $
//
// $Revision: 1.1.1.1 $
//
// $Log: IOAttrib.h,v $
// Revision 1.1.1.1  2006/11/07 08:39:48  epaud
// Imported using TkCVS
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//=============================================================================
#ifndef _IOATTRIB_H
#define _IOATTRIB_H

#include <SYBPM_SparkAll.h>

namespace SYBPM_SparkAll_ns
{

	class BooleanSpectrumAttrib: public Tango::SpectrumAttr
	{
		public:
			BooleanSpectrumAttrib(const char *name,long data_type,Tango::AttrWriteType w_type, long max_x)
				:SpectrumAttr(name,data_type,w_type,max_x) {};
			BooleanSpectrumAttrib(const char *name,long data_type,long max_x)
				:SpectrumAttr(name,data_type,max_x) {};
			~BooleanSpectrumAttrib() {};
	
			virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
			{(static_cast<SYBPM_SparkAll *>(dev))->readBooleanSpectrumAttribut(att);}
	};

	class DoubleSpectrumAttrib: public Tango::SpectrumAttr
	{
		public:
			DoubleSpectrumAttrib(const char *name,long data_type,Tango::AttrWriteType w_type, long max_x)
				:SpectrumAttr(name,data_type,w_type,max_x) {};
			DoubleSpectrumAttrib(const char *name,long data_type,long max_x)
				:SpectrumAttr(name,data_type,max_x) {};
			~DoubleSpectrumAttrib() {};
	
			virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
			{(static_cast<SYBPM_SparkAll *>(dev))->readDoubleSpectrumAttribut(att);}
	};

	class DoubleImageAttrib: public Tango::ImageAttr
	{
		public:
			DoubleImageAttrib(const char *name,long data_type,Tango::AttrWriteType w_type, long max_x, long max_y)
				:ImageAttr(name,data_type,w_type,max_x, max_y) {};
			DoubleImageAttrib(const char *name,long data_type,long max_x, long max_y)
				:ImageAttr(name,data_type,max_x, max_y) {};
			~DoubleImageAttrib() {};
	
			virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
			{(static_cast<SYBPM_SparkAll *>(dev))->readDoubleImageAttribut(att);}
	};

	class ULongImageAttrib: public Tango::ImageAttr
	{
		public:
			ULongImageAttrib(const char *name,long data_type,Tango::AttrWriteType w_type, long max_x, long max_y)
				:ImageAttr(name,data_type,w_type,max_x, max_y) {};
			ULongImageAttrib(const char *name,long data_type,long max_x, long max_y)
				:ImageAttr(name,data_type,max_x, max_y) {};
			~ULongImageAttrib() {};
	
			virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
			{(static_cast<SYBPM_SparkAll *>(dev))->readULongImageAttribut(att);}
	};

	class ShortImageAttrib: public Tango::ImageAttr
	{
		public:
			ShortImageAttrib(const char *name,long data_type,Tango::AttrWriteType w_type, long max_x, long max_y)
				:ImageAttr(name,data_type,w_type,max_x, max_y) {};
			ShortImageAttrib(const char *name,long data_type,long max_x, long max_y)
				:ImageAttr(name,data_type,max_x, max_y) {};
			~ShortImageAttrib() {};
	
			virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
			{(static_cast<SYBPM_SparkAll *>(dev))->readShortImageAttribut(att);}
	};

	class LongImageAttrib: public Tango::ImageAttr
	{
		public:
			LongImageAttrib(const char *name,long data_type,Tango::AttrWriteType w_type, long max_x, long max_y)
				:ImageAttr(name,data_type,w_type,max_x, max_y) {};
			LongImageAttrib(const char *name,long data_type,long max_x, long max_y)
				:ImageAttr(name,data_type,max_x, max_y) {};
			~LongImageAttrib() {};
	
			virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
			{(static_cast<SYBPM_SparkAll *>(dev))->readLongImageAttribut(att);}
	};

	class LongSpectrumAttrib: public Tango::SpectrumAttr
	{
		public:
			LongSpectrumAttrib(const char *name,long data_type,Tango::AttrWriteType w_type, long max_x)
				:SpectrumAttr(name,data_type,w_type,max_x) {};
			LongSpectrumAttrib(const char *name,long data_type,long max_x)
				:SpectrumAttr(name,data_type,max_x) {};
			~LongSpectrumAttrib() {};
	
			virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
			{(static_cast<SYBPM_SparkAll *>(dev))->readLongSpectrumAttribut(att);}
	};

	class ULongSpectrumAttrib: public Tango::SpectrumAttr
	{
		public:
			ULongSpectrumAttrib(const char *name,long data_type,Tango::AttrWriteType w_type, long max_x)
				:SpectrumAttr(name,data_type,w_type,max_x) {};
			ULongSpectrumAttrib(const char *name,long data_type,long max_x)
				:SpectrumAttr(name,data_type,max_x) {};
			~ULongSpectrumAttrib() {};
	
			virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
			{(static_cast<SYBPM_SparkAll *>(dev))->readULongSpectrumAttribut(att);}
	};

}

#endif	// _IOATTRIB_H
