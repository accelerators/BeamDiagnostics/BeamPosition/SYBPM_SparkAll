
//+=============================================================================
//
// file :         utils.h
//
// description :  General purpose class to measure utils betwwen triggers
//
// project :      
//
// $Author: epaud $
//
// $Revision: 1.3 $
//
// $Log: utils.h,v $
// Revision 1.3  2009/04/07 14:29:45  epaud
// See *.cpp
//
// Revision 1.2  2008/11/27 12:50:20  epaud
// *** empty log message ***
//
// Revision 1.1.1.1  2008/11/27 10:23:21  epaud
// Initial version
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#ifndef _SYBPM_utils_
#define _SYBPM_utils_

#include <iostream>
// #include <common.h>


using namespace std;

class _SYBPM_utils
{
	protected:
		long			debugFlags;

	public:
		_SYBPM_utils()
		{
			debugFlags = 0;
		};


		void setDebugFlags(long _debugFlags)
		{
			debugFlags = _debugFlags;
		};


		//+----------------------------------------------------------------------------
		//
		// method : 		utils::devicename2CellBpm
		// 
		// description : 	Extract the cell number from 1 to 38 and bpm number ( 0 = QF, 1 = QD) from the device name
		//
		//					Reminder: Internally the data are arranged as 2 rows of 27 colomns starting at QF1 and QD1
		//
		//-----------------------------------------------------------------------------
		long devicename2CellBpm(string devicename, long *cell, long *bpm)
		{
			long pos;

			if (debugFlags) cout << "_SYBPM_utils::devicename2CellBpm.  devicename: " << devicename ;

			*cell = *bpm = 0;
			pos = devicename.rfind("/");
			if (pos == -1)  return(NOT_OK);

			string bpm_nb = devicename.substr(pos+1);
			
			string bpm_str = bpm_nb.substr(0,2); 
			if (bpm_str == "qf") *bpm = 1; 
			else if (bpm_str == "qd") *bpm = 2; 
			else return(NOT_OK);	

			// if the name is postfixed with "-something" (e.g sr/d-bpmlibera-sp/qd2-stripline) then ignore that device
			if ( (long) bpm_nb.rfind("-") != -1) return(NOT_OK);

			*cell = atoi(bpm_nb.substr(2).c_str());

			if (debugFlags) cout << "devicename: " << devicename << ", cell: " << *cell << ", bpm: " << *bpm << endl;

			return(OK);
		}

		//+----------------------------------------------------------------------------
		//
		// method : 		utils::CellBpm2Devicename
		// 
		// description : 	Compute the devicename from the cell and bpm number
		//
		//					Reminder: Internally the data are arranged as 2 rows of 27 colomns starting at QF1 and QD1
		//
		//-----------------------------------------------------------------------------
		long CellBpm2Devicename(string root_devicename, long cell, long bpm, string *devicename)
		{
			if (debugFlags)
				cout << "_SYBPM_utils::CellBpm2Devicename. root_devicename: " << root_devicename << ", cell: " << cell << ", bpm: " << bpm;

			char l_devicename[80];
			sprintf(l_devicename, "%s/%s%d", root_devicename.c_str(), (bpm == 1) ? "qf" : "qd", cell );
			*devicename = l_devicename; 

			if (debugFlags)
				 cout << ", devicename: " << *devicename << endl ;
			return(OK);
		}


		//+----------------------------------------------------------------------------
		//
		// method : 		utils::indice2CellBpm
		// 
		// description : 	Compute the cell and bpm number from an indice within an array [0..xxx]
		//
		//					Reminder: Internally the data are arranged as 2 rows of 27 colomns starting at QF1 and QD1
		//
		//-----------------------------------------------------------------------------
		long indice2CellBpm(long indice, long *cell, long *bpm)
		{
			if (debugFlags) cout << "_SYBPM_utils::indice2CellBpm. indice: " << indice;

			*cell = (indice / NUMBER_OF_BPM_PER_CELL) + 1; 
			*bpm  = (indice % NUMBER_OF_BPM_PER_CELL) + 1;

			if (debugFlags) cout << ", cell: " << *cell << ", bpm: " << *bpm << endl;

			return(OK);
		}


		//+----------------------------------------------------------------------------
		//
		// method : 		utils::CellBpm2Indice
		// 
		// description : 	Compute an array [0..xxx] indice from the cell and bpm number
		//
		//					Reminder: Internally the data are arranged as 2 rows of 27 colomns starting at QF1 and QD1
		//
		//-----------------------------------------------------------------------------
		long CellBpm2Indice(long cell, long bpm, long *indice)
		{
			if (debugFlags) cout << "_SYBPM_utils::CellBpm2Indice. cell: " << cell << ", bpm: " << bpm;

			*indice = (cell -1) * NUMBER_OF_BPM_PER_CELL + (bpm-1);

			if (debugFlags) cout << ", indice: " << *indice << endl;

			return(OK);
		}

    //+----------------------------------------------------------------------------
    //
    // method : 		utils::getShortName
    //
    // description : 	Return short name (last field)
    //
    //-----------------------------------------------------------------------------
    string getShortName(const char *devName) {
		  const char *p = strrchr(devName,'/');
		  if(p) return string(p+1);
		  else return string(devName);
		}

		//+----------------------------------------------------------------------------
		//
		// method : 		utils::checkBPMName
		// 
		// description : 	method to check if the BPMname is correct
		//
		//					Reminder: Internally the data are arranged as 2 rows of 27 colomns starting at QF1 and QD1
		//
		//-----------------------------------------------------------------------------
		long checkBPMName(string BPMname, string function, int line)
		{
			TangoSys_OMemStream r, d, o;
			long cell, bpm;

			o << function << " [" << __FILE__ << "::" << line << "]" << std::ends;

			// The BPM name syntax should be of the form "cxx-y"  (e.g. c5-3)
			//----------------------------------------------------------------------------------
			if ( (BPMname.rfind("qf") == string::npos) && (BPMname.rfind("qd") == string::npos) ) {
				d << "ERROR1: BPM number should be of the form \"qfxx\" or \"qdxx\"  (e.g. qd3, qf12)";
				Tango::Except::throw_exception(r.str(), d.str(), o.str());
				return(NOT_OK);
			}
			else if ( BPMname.substr(1,1) == "0") {
				d << "ERROR3: There should be no leading \"0\" within the cell number  (e.g. qd3, qf12)";
				Tango::Except::throw_exception(r.str(), d.str(), o.str());
				return(NOT_OK);
			}
			else {
				string bpm_str = BPMname.substr(0,2); 
				bpm =  (bpm_str == "qf") ? 1 : 2; 
				cell = atoi(BPMname.substr(2).c_str());
				if ( (cell < 1) || (cell > NUMBER_OF_CELLS) ) {
					d << "ERROR4: Cell number should be between 1 and 38";
					return(NOT_OK);
				}
			}
			return(OK) ;
		}





};

#endif
