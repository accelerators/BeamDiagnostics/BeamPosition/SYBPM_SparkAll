/*----- PROTECTED REGION ID(SYBPM_SparkAll.h) ENABLED START -----*/
//=============================================================================
//
// file :        SYBPM_SparkAll.h
//
// description : Include file for the SYBPM_SparkAll class
//
// project :     SYBPM_SparkAll
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef SYBPM_SparkAll_H
#define SYBPM_SparkAll_H

#include <tango.h>


const double	MAX_ADC_ACQUISITION_DURATION = 0.1;				// Maximum Acquistion duration when the ADC are Enable
const double	MAX_ACQUISITION_DURATION_DECIMATION = 50.0;		// Maximum Acquistion duration when Decimation is Enable 
const double	MAX_ACQUISITION_DURATION_NO_DECIMATION = 5.0;	// Maximum Acquistion duration when Decimation is NOT Enable 
// const long		NUMBER_OF_SA_SAMPLES 	= 10;					// For tests
//const long		NUMBER_OF_SA_SAMPLES 	= 3600;					// 3600 samples at 40 Hz -> 1'30" recording 
const long		NUMBER_OF_SA_SAMPLES 	= 7200;					// 7200 samples at 40 Hz -> 3'00" recording 

const long	ERRORS_STACK_SIZE          	= 50;
const long	READ_ATTR_GROUP_TIMEOUT    	= 800;  	// milli-seconds
const long	WRITE_ATTR_GROUP_TIMEOUT    = 3000; 	// milli-seconds
const long	READ_SPECT_ATTR_GROUP_TIMEOUT = 800;	// milli-seconds
const long	COMMAND_GROUP_TIMEOUT 		= 2000;		// milli-seconds
const long	MAX_NB_OF_RETRIES 			= 5;		// Nb of retries when FILL_CNT difference is only 1 

const long	NUMBER_OF_CELLS    			= 39;		// 39 sections in the Booster
const long	NUMBER_OF_BPM_PER_CELL  = 2;		// 2 BPM:  QF and QD
const long  TOTAL_BPM               = (NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL);
const long	NUMBER_OF_BPM 				  = ((NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL) -3);	// 3 BPM are missing

const double NaN						= strtod("NAN", NULL);
const long	KILO                		= 1024;
const long	MEGA                		= KILO * KILO;
const long	MEGA_HZ             		= 1000000;
const long	ADC_MAX_BUFFER_SIZE 		= 7 * MEGA;
const long	TBT_MAX_BUFFER_SIZE 		= 256 * KILO;
const long	TDP_MAX_BUFFER_SIZE 		= 60000;

const bool  NOT_OK						= 0;
const long	OK							= 1;
const long	ERROR						= -1;
const long	WARNING						= -2;
const long	INFO						= -3;
const long	TIMEOUT						= -4;


#include "SYBPMUtils.h"

//---------------------   Bits definition of the status -----------------------------------
#define LIBERA_NOT_DEFINED          	0       // (1) 	Set if the device name not found in the database (XPos, Zpos = NaN)
#define DEVICE_DISABLE          		1       // (2) 	Set if the device name found in the database, but is disable
#define LIBERA_SERVER_NOT_REACHABLE     2       // (4) 	Set if the server doesn't answer to a ping command  (XPos, Zpos = NaN)
#define LIBERA_IN_FAULT                 3       // (8) 	Set if the server status is not OK (Libera in FAULT)
#define DEVICE_TIMEOUT                  4       // (16) Set if the server returns a timeout exceprt for reading X and Z positions 
#define XPOS_DEVICE_TIMEOUT				5 		// (32) Set if the server returns a timeout when reading Xpos 
#define ZPOS_DEVICE_TIMEOUT				6 		// (64) Set if the server returns a timeout when reading Zpos 
#define LIBERA_IN_ALARM					7 		// (128) Set if the server returns a Incoherency ALARM 
#define UNDER_THRESHOLD					8 		// (256) Not treated at the moment
#define HARDWARE_ALARM					9 		// (512) Hardware alarm detected on the Libera (Temp too high or fan speeds too low)
#define LIBERA_FROZEN					10 		// (1024) Same values detected on X or Z 


// Bits assignment within the DebugFlags
//-------------------------------------------------------------------------------------
const long CLASS_IO                 =  0;   // (1)		print the Input/output from the functions
const long CMD_IO               	=  1;   // (2)		print before each group call
const long ATTRIBUTES_IO            =  2;   // (4)		print the Input/output of the attribute calls
const long DUMP_READATTR_INTERNAL   =  3;   // (8)		print inside attributes reading

const long DUMP_PERFORMANCE  		=  4;  	// (16)		print performance informations
const long MUTEX			 		=  5; 	// (32)		print Mutex results
const long PARAMS_SURVEY 			=  6; 	// (64)		print the results of the parameters survey
const long TIMING 					=  7; 	// (128)	print timing issues 
const long CHK_NEWDATA 				=  8; 	// (256)	print check for new data issues		


// General macros to take and release the SharedDataMutex
//-------------------------------------------------------------------------------------------

#define PRINT_WARN_STREAM

#ifdef PRINT_WARN_STREAM
#define _PSDATA_OMNI_MUTEX_ \
	WARN_STREAM << __FILE__  << "::" <<  __LINE__ <<"> ParamSurveySharedDataMutex try to take"; \
	omni_mutex_lock mx(ParamSurveySharedDataMutex); \
	WARN_STREAM << __FILE__  << "::" <<  __LINE__ <<"> ParamSurveySharedDataMutex got";  

#define _PSDATA_OMNI_MUTEX_REL_ \
	WARN_STREAM << __FILE__  << "::" <<  __LINE__ <<"> ParamSurveySharedDataMutex release (end of block)";  

#else

#define _PSDATA_OMNI_MUTEX_ \
	cout << "l" << flush; \
	omni_mutex_lock mx(ParamSurveySharedDataMutex); \
	cout << "g" << flush; \

#define _PSDATA_OMNI_MUTEX_REL_ \
	cout << "u" << flush; 
#endif


/*----- PROTECTED REGION END -----*/	//	SYBPM_SparkAll.h

/**
 *  SYBPM_SparkAll class description:
 *    This class allows to manage and survey the Libera Spark devices of the ESRF booster.
 */

namespace SYBPM_SparkAll_ns
{
/*----- PROTECTED REGION ID(SYBPM_SparkAll::Additional Class Declarations) ENABLED START -----*/

//	Additional Class Declarations
class _ParamSurveyThread;
class _checkNewDataThread;

/*----- PROTECTED REGION END -----*/	//	SYBPM_SparkAll::Additional Class Declarations

class SYBPM_SparkAll : public TANGO_BASE_CLASS
{

/*----- PROTECTED REGION ID(SYBPM_SparkAll::Data Members) ENABLED START -----*/

//	Add your own data members

/*----- PROTECTED REGION END -----*/	//	SYBPM_SparkAll::Data Members

//	Device property data members
public:
	//	DevicenameFilter:	Add only the devices starting with this string to the Libera
	//  Spark group to manage
	string	devicenameFilter;
	//	DisableList:	Liste of disable Libera Spark devices
	string	disableList;
	//	SurveyPeriod:	Period (in seconds) at which the parameters are checked on all the defined
	//  Libera Spark devices.
	Tango::DevULong	surveyPeriod;
	//	ADC_SamplingFrequency:	ADC sampling frequency en MHz.
	//  This is needed here to calculate the ADC and TBT buffers size
	Tango::DevDouble	aDC_SamplingFrequency;
	//	SendEventOnFillCounterChange:	If this property is true, an event is sent when the 
	//  FILL_CNT changes on the LiberaSpark
	Tango::DevBoolean	sendEventOnFillCounterChange;
	//	DeviceList:	List of BPM devices
	vector<string>	deviceList;

//	Attribute data members
public:
	Tango::DevBoolean	*attr_ADC_Enable_read;
	Tango::DevBoolean	*attr_AMPL_Enable_read;
	Tango::DevBoolean	*attr_IQ_Enable_read;
	Tango::DevBoolean	*attr_QSUM_Enable_read;
	Tango::DevBoolean	*attr_Decimation_Enable_read;
	Tango::DevBoolean	*attr_TDP_AMPL_Enable_read;
	Tango::DevBoolean	*attr_TDP_QSUM_Enable_read;
	Tango::DevBoolean	*attr_TDP_XZ_Enable_read;
	Tango::DevULong	*attr_MAFLength_read;
	Tango::DevULong	*attr_ADC_BufferSize_read;
	Tango::DevDouble	*attr_AcquisitionDuration_read;
	Tango::DevULong	*attr_TBT_BufferSize_read;
	Tango::DevULong	*attr_debugFlags_read;
	Tango::DevULong	*attr_FillCounter_read;
	Tango::DevString	*attr_ConfigurationFilename_read;
	Tango::DevULong	*attr_QSUM_Threshold_read;
	Tango::DevBoolean	*attr_ExpertMode_read;
	Tango::DevBoolean	*attr_SA_Enable_read;
	Tango::DevShort	*attr_SignalProcessing_read;
	Tango::DevString	*attr_LastErrors_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	SYBPM_SparkAll(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	SYBPM_SparkAll(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	SYBPM_SparkAll(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */
	~SYBPM_SparkAll() {delete_device();};


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : SYBPM_SparkAll::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(vector<long> &attr_list);
	//--------------------------------------------------------
	/*
	 *	Method      : SYBPM_SparkAll::write_attr_hardware()
	 *	Description : Hardware writing for attributes.
	 */
	//--------------------------------------------------------
	virtual void write_attr_hardware(vector<long> &attr_list);

/**
 *	Attribute ADC_Enable related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_ADC_Enable(Tango::Attribute &attr);
	virtual void write_ADC_Enable(Tango::WAttribute &attr);
	virtual bool is_ADC_Enable_allowed(Tango::AttReqType type);
/**
 *	Attribute AMPL_Enable related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_AMPL_Enable(Tango::Attribute &attr);
	virtual void write_AMPL_Enable(Tango::WAttribute &attr);
	virtual bool is_AMPL_Enable_allowed(Tango::AttReqType type);
/**
 *	Attribute IQ_Enable related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_IQ_Enable(Tango::Attribute &attr);
	virtual void write_IQ_Enable(Tango::WAttribute &attr);
	virtual bool is_IQ_Enable_allowed(Tango::AttReqType type);
/**
 *	Attribute QSUM_Enable related methods
 *	Description: If False: The QSUM is not read and the X and Z positions are always calculated (even without beam)
 *               If thue:  The QSUM is read and the X and Z positions is calculated only if the QSUM is greatter than the QSUM_Threshold
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_QSUM_Enable(Tango::Attribute &attr);
	virtual void write_QSUM_Enable(Tango::WAttribute &attr);
	virtual bool is_QSUM_Enable_allowed(Tango::AttReqType type);
/**
 *	Attribute Decimation_Enable related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_Decimation_Enable(Tango::Attribute &attr);
	virtual void write_Decimation_Enable(Tango::WAttribute &attr);
	virtual bool is_Decimation_Enable_allowed(Tango::AttReqType type);
/**
 *	Attribute TDP_AMPL_Enable related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_TDP_AMPL_Enable(Tango::Attribute &attr);
	virtual void write_TDP_AMPL_Enable(Tango::WAttribute &attr);
	virtual bool is_TDP_AMPL_Enable_allowed(Tango::AttReqType type);
/**
 *	Attribute TDP_QSUM_Enable related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_TDP_QSUM_Enable(Tango::Attribute &attr);
	virtual void write_TDP_QSUM_Enable(Tango::WAttribute &attr);
	virtual bool is_TDP_QSUM_Enable_allowed(Tango::AttReqType type);
/**
 *	Attribute TDP_XZ_Enable related methods
 *	Description: 
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_TDP_XZ_Enable(Tango::Attribute &attr);
	virtual void write_TDP_XZ_Enable(Tango::WAttribute &attr);
	virtual bool is_TDP_XZ_Enable_allowed(Tango::AttReqType type);
/**
 *	Attribute MAFLength related methods
 *	Description: MAF Filter length in ADC clock unit
 *
 *	Data type:	Tango::DevULong
 *	Attr type:	Scalar
 */
	virtual void read_MAFLength(Tango::Attribute &attr);
	virtual void write_MAFLength(Tango::WAttribute &attr);
	virtual bool is_MAFLength_allowed(Tango::AttReqType type);
/**
 *	Attribute ADC_BufferSize related methods
 *	Description: Size of the ADC buffer size (max value is 1M samples).
 *               It is automatically calculated from the ADC_SamplesFrequency and AcquisitionDuration.
 *               The ADC_SIZE send to the Libera Spark is 4 times this values (ADC_SIZE 
 *               is the buffer size on the Libera Spark used by the 4 ADCs)
 *
 *	Data type:	Tango::DevULong
 *	Attr type:	Scalar
 */
	virtual void read_ADC_BufferSize(Tango::Attribute &attr);
	virtual bool is_ADC_BufferSize_allowed(Tango::AttReqType type);
/**
 *	Attribute AcquisitionDuration related methods
 *	Description: Acquisition duration in milli-sec. The number of ADC samples (ADC_SIZE) is
 *               calculated by taking care of the ADC_SampleFrequency (~100 MHz).
 *               This is the time after which the data are available after the trigger (when 
 *               FILL_CNT is incremented)
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
	virtual void read_AcquisitionDuration(Tango::Attribute &attr);
	virtual void write_AcquisitionDuration(Tango::WAttribute &attr);
	virtual bool is_AcquisitionDuration_allowed(Tango::AttReqType type);
/**
 *	Attribute TBT_BufferSize related methods
 *	Description: Size of the TBT buffer size (max value is 256K samples).
 *               It is automatically calculated from the TBT_Frequency and AcquisitionDuration.
 *
 *	Data type:	Tango::DevULong
 *	Attr type:	Scalar
 */
	virtual void read_TBT_BufferSize(Tango::Attribute &attr);
	virtual bool is_TBT_BufferSize_allowed(Tango::AttReqType type);
/**
 *	Attribute debugFlags related methods
 *	Description: DEBUG flags bit fields:
 *               - 0 (1):  Classes IO
 *               - 1 (2)   Commands IO
 *               - 2 (4)   Attributes IO
 *               - 3 (8)   print inside attributes reading
 *               
 *               - 4 (16)     print performance informations
 *               - 5 (32)     print Mutex results
 *               - 6 (64)     print the results of the parameters survey
 *               - 7 (128)   Print timing issue
 *
 *	Data type:	Tango::DevULong
 *	Attr type:	Scalar
 */
	virtual void read_debugFlags(Tango::Attribute &attr);
	virtual void write_debugFlags(Tango::WAttribute &attr);
	virtual bool is_debugFlags_allowed(Tango::AttReqType type);
/**
 *	Attribute FillCounter related methods
 *	Description: Number of triggers received
 *
 *	Data type:	Tango::DevULong
 *	Attr type:	Scalar
 */
	virtual void read_FillCounter(Tango::Attribute &attr);
	virtual bool is_FillCounter_allowed(Tango::AttReqType type);
/**
 *	Attribute ConfigurationFilename related methods
 *	Description: 
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_ConfigurationFilename(Tango::Attribute &attr);
	virtual void write_ConfigurationFilename(Tango::WAttribute &attr);
	virtual bool is_ConfigurationFilename_allowed(Tango::AttReqType type);
/**
 *	Attribute QSUM_Threshold related methods
 *	Description: If False: The X and Z positions are always calculated (even without beam)
 *               If true:  The X and Z positions is calculated only if the QSUM is greatter than the QSUM_Threshold
 *
 *	Data type:	Tango::DevULong
 *	Attr type:	Scalar
 */
	virtual void read_QSUM_Threshold(Tango::Attribute &attr);
	virtual void write_QSUM_Threshold(Tango::WAttribute &attr);
	virtual bool is_QSUM_Threshold_allowed(Tango::AttReqType type);
/**
 *	Attribute ExpertMode related methods
 *	Description: When True the system is no longet protected to bad parameters setting which can saturate
 *               the network and crashes Spark server.
 *               The `EXPERT` should be fully aware of what (s)he is doing !!!! Otherwise it`s not an expert
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_ExpertMode(Tango::Attribute &attr);
	virtual void write_ExpertMode(Tango::WAttribute &attr);
	virtual bool is_ExpertMode_allowed(Tango::AttReqType type);
/**
 *	Attribute SA_Enable related methods
 *	Description: Enable the the SA mode
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_SA_Enable(Tango::Attribute &attr);
	virtual void write_SA_Enable(Tango::WAttribute &attr);
	virtual bool is_SA_Enable_allowed(Tango::AttReqType type);
/**
 *	Attribute SignalProcessing related methods
 *	Description: Set the Signal Processing Algorithm:
 *                 0:  TDP
 *                 1:  TBT (default value in USM)
 *
 *	Data type:	Tango::DevShort
 *	Attr type:	Scalar
 */
	virtual void read_SignalProcessing(Tango::Attribute &attr);
	virtual void write_SignalProcessing(Tango::WAttribute &attr);
	virtual bool is_SignalProcessing_allowed(Tango::AttReqType type);
/**
 *	Attribute LastErrors related methods
 *	Description: 
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Spectrum max = 64
 */
	virtual void read_LastErrors(Tango::Attribute &attr);
	virtual bool is_LastErrors_allowed(Tango::AttReqType type);


	//--------------------------------------------------------
	/**
	 *	Method      : SYBPM_SparkAll::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();




//	Command related methods
public:
	/**
	 *	Command State related method
	 *	Description: This command gets the device state (stored in its device_state data member) and returns it to the caller.
	 *
	 *	@returns Device state
	 */
	virtual Tango::DevState dev_state();
	/**
	 *	Command DisableBPM related method
	 *	Description: Command to disable a BPM
	 *
	 *	@param argin 
	 */
	virtual void disable_bpm(Tango::DevString argin);
	virtual bool is_DisableBPM_allowed(const CORBA::Any &any);
	/**
	 *	Command EnableBPM related method
	 *	Description: Command to enable a BPM
	 *
	 *	@param argin 
	 */
	virtual void enable_bpm(Tango::DevString argin);
	virtual bool is_EnableBPM_allowed(const CORBA::Any &any);
	/**
	 *	Command GetBPMStatus related method
	 *	Description: Gives the status of all the defined BPM
	 *
	 *	@returns 
	 */
	virtual Tango::DevVarStringArray *get_bpmstatus();
	virtual bool is_GetBPMStatus_allowed(const CORBA::Any &any);
	/**
	 *	Command GetLastErrors related method
	 *	Description: Returns the errors stack.
	 *
	 *	@returns 
	 */
	virtual Tango::DevVarStringArray *get_last_errors();
	virtual bool is_GetLastErrors_allowed(const CORBA::Any &any);
	/**
	 *	Command ResetErrors related method
	 *	Description: Reset the errors (empty the errors stack)
	 *
	 */
	virtual void reset_errors();
	virtual bool is_ResetErrors_allowed(const CORBA::Any &any);
	/**
	 *	Command ResetFillCounter related method
	 *	Description: Reset the FILL counter on all the defined LiberaSpark
	 *
	 */
	virtual void reset_fill_counter();
	virtual bool is_ResetFillCounter_allowed(const CORBA::Any &any);
	/**
	 *	Command SendSoftwareTrigger related method
	 *	Description: Send a software trigger on all the Libera Spark
	 *
	 */
	virtual void send_software_trigger();
	virtual bool is_SendSoftwareTrigger_allowed(const CORBA::Any &any);
	/**
	 *	Command ResetPositionsReferences related method
	 *	Description: Set the X position and Z position references to 0
	 *
	 */
	virtual void reset_positions_references();
	virtual bool is_ResetPositionsReferences_allowed(const CORBA::Any &any);
	/**
	 *	Command SetPositionsReferences related method
	 *	Description: Set the X position and Z position references to the actual values
	 *
	 */
	virtual void set_positions_references();
	virtual bool is_SetPositionsReferences_allowed(const CORBA::Any &any);
	/**
	 *	Command ResetDebugFlags related method
	 *	Description: Remove all debug flags of spark devices
	 *
	 */
	virtual void reset_debug_flags();
	virtual bool is_ResetDebugFlags_allowed(const CORBA::Any &any);


	//--------------------------------------------------------
	/**
	 *	Method      : SYBPM_SparkAll::add_dynamic_commands()
	 *	Description : Add dynamic commands if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_commands();

/*----- PROTECTED REGION ID(SYBPM_SparkAll::Additional Method prototypes) ENABLED START -----*/

  // For dynamics attributes
  //-------------------------------------------------------------------------------------------
  void createBooleanSpectrumAttribut(string name, long size, bool expert);
  void readBooleanSpectrumAttribut(Tango::Attribute &attr);
  void createLongSpectrumAttribut(string name, long size, string unit, string standard_unit, string display_unit,
                                  string min_value, string max_value, string format, bool expert);
  void readLongSpectrumAttribut(Tango::Attribute &attr);
  void createULongSpectrumAttribut(string name, long size, string unit, string standard_unit, string display_unit,
                                   string min_value, string max_value, string format, bool expert);
  void readULongSpectrumAttribut(Tango::Attribute &attr);
  void createDoubleSpectrumAttribut(string name, long size, string unit, string standard_unit, string display_unit,
                                    string min_value, string max_value, string format, bool expert);
  void readDoubleSpectrumAttribut(Tango::Attribute &attr);
  void createDoubleImageAttribut(string name, long x_size, long y_size, string unit, string standard_unit,
                                 string display_unit, string min_value, string max_value, string format, bool expert);
  void readDoubleImageAttribut(Tango::Attribute &attr);
  void createULongImageAttribut(string name, long x_size, long y_size, string unit, string standard_unit,
                                string display_unit, string min_value, string max_value, string format, bool expert);
  void readULongImageAttribut(Tango::Attribute &attr);
  void createShortImageAttribut(string name, long x_size, long y_size, string unit, string standard_unit,
                                string display_unit, string min_value, string max_value, string format, bool expert);
  void readShortImageAttribut(Tango::Attribute &attr);
  void createLongImageAttribut(string name, long x_size, long y_size, string unit, string standard_unit,
                               string display_unit, string min_value, string max_value, string format, bool expert);
  void readLongImageAttribut(Tango::Attribute &attr);
  bool readBooleanAttributeMemorizedValue(string attribute_name);
  double readAttributeMemorizedValue(string attribute_name);
  void writeAttributeMemorizedValue(string attribute_name, double newvalue);
  void attributeSetWriteValue(string attribute_name, Tango::DevULong value);
  long readLiberaBPMStatus();                          // Read the state of each defined devices

  template<class IN_POINTER_TYPE, class OUT_POINTER_TYPE>
  void squeezeBpmBufferFirstIsQF1(IN_POINTER_TYPE *p_buffer, OUT_POINTER_TYPE *p_SparkData);
  template<class IN_POINTER_TYPE, class OUT_POINTER_TYPE>
  void squeezeBpmBuffer(IN_POINTER_TYPE *p_buffer, OUT_POINTER_TYPE *p_SparkData);
  template<class IN_POINTER_TYPE, class OUT_POINTER_TYPE>
  void squeezeBpmBuffer(IN_POINTER_TYPE *p_buffer, OUT_POINTER_TYPE *p_SparkData, double scale);
  template<class IN_POINTER_TYPE, class OUT_POINTER_TYPE, class DATA_TYPE>
  void squeezeBpmBuffer_2D(IN_POINTER_TYPE *p_buffer, OUT_POINTER_TYPE *p_SparkData, unsigned long _bufferSize,
                           unsigned long _number_of_data, DATA_TYPE data_type);

  void set_property(string name, string value);
  void resetErrorsStack();
  void addError(string call);
  void dumpErrorsStack();
  long deviceDisable(string devicename);                // Check if the device is disable in the DisableList property
  void throw_exception(long severity, string error_message, string function, int line);
  void read_All_XPosSAHistory(double *p_SparkData);
  void read_All_ZPosSAHistory(double *p_SparkData);
  void read_All_SumSAHistory(Tango::DevULong *p_SparkData);
  bool readAttributeDouble(string attr_name, Tango::DevDouble *p_buffer, double default_value);
  void sendCommand(string cmd_name);

  long init_errors;
  omni_mutex ErrorsMutex;
  vector<string> errors_list;          // Errors stack
  Tango::Group *LiberaSparkGroup;        // TANGO group of the found devices
  unsigned long debugFlags;
  bool init_completed;
  bool ExpertMode;
  string ConfigurationFilename;          // Configuration Filename

  _SYBPM_utils utils;                             // General purposes methods

  friend class _ParamSurveyThread;
  _ParamSurveyThread *ParamSurveyThread;

  friend class _checkNewDataThread;
  _checkNewDataThread *checkNewDataThread;

  long faults;
  double tBT_SamplingFrequency;

  omni_mutex ParamSurveySharedDataMutex;        // Mutex to be locked when accessing the BPMLiberaAll class
  struct s_ParamSurveySharedData {

    unsigned long debugFlags;
    bool sendEventOnFillCounterChange;
    bool ParamSurveyThreadStarted;
    bool checkNewDataThreadStarted;
    bool thread_suicide;
    long ParamSurveyThreadpollingPeriod_in_us;
    bool sparkTriggerMissed;

    Tango::DevULong Status[NUMBER_OF_CELLS][NUMBER_OF_BPM_PER_CELL];            // Status vector
    bool *p_ADC_Enable_buffer;
    bool *p_AMPL_Enable_buffer;
    bool *p_QSUM_Enable_buffer;
    bool *p_SA_Enable_buffer;
    bool *p_IQ_Enable_buffer;
    bool *p_Decimation_Enable_buffer;
    bool *p_TDP_AMPL_Enable_buffer;
    bool *p_TDP_QSUM_Enable_buffer;
    bool *p_TDP_IQ_Enable_buffer;

    Tango::DevLong *p_SignalProcessing_buffer;

    Tango::DevULong *p_MAFLength_buffer;
    Tango::DevULong *p_ADC_BufferSize_buffer;
    Tango::DevULong *p_TBT_BufferSize_buffer;
    Tango::DevULong *p_FillCounter_buffer;
    Tango::DevULong *p_Status_buffer;

    Tango::DevDouble *p_AcquisitionDuration_buffer;
    Tango::DevDouble *p_TBT_XPosition_Mean_buffer;
    Tango::DevDouble *p_TBT_ZPosition_Mean_buffer;
    Tango::DevDouble *p_TBT_XPositionRef_buffer;
    Tango::DevDouble *p_TBT_ZPositionRef_buffer;
    Tango::DevDouble *p_TBT_XPositionMinusRef_buffer;
    Tango::DevDouble *p_TBT_ZPositionMinusRef_buffer;
    Tango::DevDouble *p_TBT_Sum_Mean_buffer;
    Tango::DevDouble *p_TBT_SumRef_buffer;
    Tango::DevDouble *p_TBT_SumMinusRef_buffer;
    Tango::DevDouble *p_TBT_Va_Mean_buffer;
    Tango::DevDouble *p_TBT_Vb_Mean_buffer;
    Tango::DevDouble *p_TBT_Vc_Mean_buffer;
    Tango::DevDouble *p_TBT_Vd_Mean_buffer;

    bool ADC_Enable;
    bool ADC_Enable_Coherent;
    bool SA_Enable;
    bool SA_Enable_Coherent;
    bool AMPL_Enable;
    bool AMPL_Enable_Coherent;
    bool QSUM_Enable;
    bool QSUM_Enable_Coherent;
    bool IQ_Enable;
    bool IQ_Enable_Coherent;
    bool Decimation_Enable;
    bool Decimation_Enable_Coherent;
    bool TDP_AMPL_Enable;
    bool TDP_AMPL_Enable_Coherent;
    bool TDP_QSUM_Enable;
    bool TDP_QSUM_Enable_Coherent;
    bool TDP_IQ_Enable;
    bool TDP_IQ_Enable_Coherent;
    Tango::DevLong SignalProcessing;
    bool SignalProcessing_Coherent;
    Tango::DevULong MAFLength;
    bool MAFLength_Coherent;
    Tango::DevULong ADC_BufferSize;
    bool ADC_BufferSize_Coherent;
    Tango::DevDouble AcquisitionDuration;
    bool AcquisitionDuration_Coherent;
    Tango::DevULong TBT_BufferSize;
    bool TBT_BufferSize_Coherent;
    long TDP_BufferSize;
    Tango::DevULong FillCounter;
    Tango::DevULong PreviousFillCounter;
    bool FillCounter_Coherent;
    Tango::DevULong QSUM_Threshold;

  };

  s_ParamSurveySharedData ParamSurveySharedData;


  // Templace Group calls
  //+----------------------------------------------------------------------------
  //
  // method : 		LiberaSparkAll::readAttribute()
  //
  // description:		Read an attribute on each defined liberas using a group call and
  //					compare with the default_value. Return true if all the values are equal to
  //					the default value.
  //					Also a buffer of all the values is returned
  //
  //-----------------------------------------------------------------------------
  template <class P_BUFFER, class DEFAULT_VALUE>
  bool readAttribute(string attr_name, P_BUFFER p_buffer, DEFAULT_VALUE default_value)
  {
    long 			size;
    DEFAULT_VALUE	value;
    P_BUFFER		pc_buffer;
    long 			cell;
    long 			bpm;
    bool 			coherent	= true;
    long 			errors 		= 0;
    string    devError;

    if ( ParamSurveySharedData.debugFlags & (1 << CLASS_IO) ) cout << "\nreadAttribute(template): " << attr_name << endl;

    LiberaSparkGroup -> set_timeout_millis(READ_SPECT_ATTR_GROUP_TIMEOUT);
    Tango::GroupAttrReplyList  Values = LiberaSparkGroup -> read_attribute (attr_name, true);

    pc_buffer = p_buffer;
    for (long i=0; i < NUMBER_OF_CELLS; i++)
      for (long j=0; j < NUMBER_OF_BPM_PER_CELL; j++)
        *pc_buffer++ = default_value;

    size = Values.size();
    if ( ParamSurveySharedData.debugFlags & (1 << DUMP_READATTR_INTERNAL ) ) cout << "number of Values: " << size << endl;

    for (long i = 0; i < size; i++) {

      if (utils.devicename2CellBpm(Values[i].dev_name(), &cell, &bpm) != OK) continue;

      // Don't treats the error if device is LIBERA_SERVER_NOT_REACHABLE
      //-----------------------------------------------------------------------
      if ((!(ParamSurveySharedData.Status[cell - 1][bpm - 1] & (1 << LIBERA_SERVER_NOT_REACHABLE))) &&
          (!(ParamSurveySharedData.Status[cell - 1][bpm - 1] & (1 << DEVICE_DISABLE)))) {

        if (!(ParamSurveySharedData.Status[cell - 1][bpm - 1] & (1 << LIBERA_SERVER_NOT_REACHABLE))) {

          if (ParamSurveySharedData.debugFlags & (1 << DUMP_READATTR_INTERNAL))
            cout << "Treating: " << Values[i].dev_name() << "::" << Values[i].obj_name() << ": ";

          try {
            value = 0;
            Values[i] >> value;
            if (ParamSurveySharedData.debugFlags & (1 << DUMP_READATTR_INTERNAL)) cout << value << endl;
            pc_buffer = p_buffer + ((cell - 1) * NUMBER_OF_BPM_PER_CELL) + (bpm - 1);
            *pc_buffer = value;
            if (value != default_value) coherent = false;
          }

          catch (const Tango::DevFailed &df) {
            if (ParamSurveySharedData.debugFlags & (1 << DUMP_READATTR_INTERNAL)) {
              string reason = df.errors[df.errors.length() - 1].reason.in();
              cout << ", Error: " << reason << endl;
            }
            devError += " " + utils.getShortName(Values[i].dev_name().c_str());
            errors++;
            coherent = false;
          }
        }
      }
    }

    if (errors) {
      string l_error = "readAttribute(" + attr_name + "): Error detected on " + devError;
      addError(l_error);
    }

    return(coherent);

  }


  //+----------------------------------------------------------------------------
  //
  // method : 		LiberaSparkAll::writeAttribute()
  //
  // description:		Write an attribute to each defined liberas  using a group call
  //
  //-----------------------------------------------------------------------------
  template <class VALUE>
  void writeAttribute(string attr_name, VALUE value)
  {
    if ( ParamSurveySharedData.debugFlags & (1 << CLASS_IO ) )   cout << "\nwriteAttribute(). " << attr_name << ": " << value << endl;

    long 	size;
    long 	cell;
    long 	bpm;

    Tango::DeviceAttribute dev_attr( attr_name, value);

    // write_attribute is slower than read_attribute, therefore put back the timeout to 3 seconds
    //--------------------------------------------------------------------------------------
    LiberaSparkGroup -> set_timeout_millis(WRITE_ATTR_GROUP_TIMEOUT);
    Tango::GroupReplyList Values = LiberaSparkGroup -> write_attribute (dev_attr);

    if (Values.has_failed()) {

      size = Values.size();
      for (long i=0; i < size; i++) {
        if (Values[i].has_failed()) {

          cout << "writeAttribute: " << attr_name << ", devicename: " << Values[i].dev_name() << endl;

          if (utils.devicename2CellBpm(Values[i].dev_name(), &cell, &bpm) != OK) continue;

          // reset the previous errors
          //---------------------------------------------------------------
          ParamSurveySharedData.Status[cell-1][bpm-1] &= ( (1 << LIBERA_NOT_DEFINED) | (1 << DEVICE_DISABLE ) );

          // Don't treats the error if device is DISABLE
          //-----------------------------------------------------------------------
          if ( ! (ParamSurveySharedData.Status[cell-1][bpm-1] & (1 << DEVICE_DISABLE) ) ) {
            const Tango::DevErrorList& l_errors_list = Values[i].get_err_stack();
            for (size_t j=0; j < l_errors_list.length(); j++) {

              string reason = l_errors_list[j].reason.in();
              cout << Values[i].dev_name() << ", " << attr_name << ". ERROR: " << reason << endl;
              if (reason == "API_CantConnectToDevice") {
                ParamSurveySharedData.Status[cell-1][bpm-1] |= (1 << LIBERA_SERVER_NOT_REACHABLE);
              }
              else if (reason == "API_AttributeFailed") {
                ParamSurveySharedData.Status[cell-1][bpm-1] |= (1 << LIBERA_IN_FAULT );
              }
              else if (reason == "API_DeviceTimedOut") {
                ParamSurveySharedData.Status[cell-1][bpm-1] |= (1 << DEVICE_TIMEOUT );
              }
            }
          }
        }
      }
    }

  }

  //+----------------------------------------------------------------------------
  //
  // method : 		LiberaSparkAll::readAttributes()
  //
  // description:		Read the X and Z position of each defined liberas  using a group
  //					call
  //
  //+----------------------------------------------------------------------------
  template <class P_BUFFER, class TYPE>
  void readAttributes(vector<string> _attr_names, P_BUFFER SparkData, TYPE type, unsigned long _nb_of_channels, unsigned long _bufferSize)
  {
    Tango::DevState     local_state;

    long 			size;
    vector<TYPE> 	values;
    long 			cell;
    long 			bpm;

    if ( (debugFlags & (1 << CLASS_IO))  ||  (debugFlags & (1 << DUMP_PERFORMANCE)) ) {
      cout << "\nreadAttributes(): ";
      for (size_t i=0; i < _nb_of_channels; i++)  cout << _attr_names[i] << ", ";
      cout << endl;
    }

    LiberaSparkGroup -> set_timeout_millis(READ_ATTR_GROUP_TIMEOUT);
    Tango::GroupAttrReplyList SparkValues = LiberaSparkGroup -> read_attributes (_attr_names, true);

    P_BUFFER p_SparkData = SparkData;
    size_t bpm_indice;
    size_t attr_indice;
    size_t buffer_indice;
    size_t buffer_offset;

    // Extracts data
    //------------------------------------------------------------------------------
    size = SparkValues.size();
    if (debugFlags & (1 << CLASS_IO))
      cout << "\treadAttributes: Returned: " << size << " values, " << size / _nb_of_channels << " Sparks\n";

    for (long i = 0; i < size; i++) {

      string deviceName = SparkValues[i].dev_name();
      string attrName = SparkValues[i].obj_name();

      if (utils.devicename2CellBpm(deviceName, &cell, &bpm) != OK) continue;
      if (ParamSurveySharedData.Status[cell-1][bpm-1] & (1 << DEVICE_DISABLE)) continue;

      try {
        SparkValues[i] >> values;

        bpm_indice = ((cell - 1) * NUMBER_OF_BPM_PER_CELL) + (bpm - 1);
        for (attr_indice = 0; attr_indice < _nb_of_channels; attr_indice++) {
          if (attrName == _attr_names[attr_indice]) break;
        }
        buffer_indice = bpm_indice + (attr_indice * NUMBER_OF_BPM_PER_CELL * NUMBER_OF_CELLS);
        buffer_offset = buffer_indice * _bufferSize;

        if (debugFlags & (1 << DUMP_READATTR_INTERNAL)) {
          cout << "Treating: " << deviceName << ", attribute: " << attrName << ". size: " << values.size();
          cout << ", bpm_indice: " << bpm_indice << ", attr_indice: " << attr_indice << ", buffer_indice: "
               << buffer_indice <<
               ", buffer_offset: " << buffer_offset << endl;
          cout << "value size: " << values.size() << ". Values: ";
          for (size_t i = 0; i < values.size(); i++) {
            if (i < 16) cout << "\t" << values[i] << ", ";
          }
          cout << endl;
        }

        p_SparkData = SparkData + buffer_offset;
        for (size_t i = 0; i < values.size(); i++) {
          *p_SparkData++ = values[i];
        }
        // reset the previous errors
        ParamSurveySharedData.Status[cell - 1][bpm - 1] &= ((1 << LIBERA_NOT_DEFINED) | (1 << DEVICE_DISABLE));
      } catch (const Tango::DevFailed &df) {

        // Only the last error is interesting
        string reason = df.errors[df.errors.length() - 1].reason.in();
        cout << deviceName << ", " << attrName << ". ERROR: " << reason << endl;

        if (reason == "API_CantConnectToDevice")
          ParamSurveySharedData.Status[cell - 1][bpm - 1] |= (1 << LIBERA_SERVER_NOT_REACHABLE);
        else if (reason == "API_AttributeFailed")
          ParamSurveySharedData.Status[cell - 1][bpm - 1] |= (1 << LIBERA_IN_FAULT);
        else if (reason == "API_DeviceTimedOut") ParamSurveySharedData.Status[cell - 1][bpm - 1] |= (1 << DEVICE_TIMEOUT);
      }

    };

  }


/*----- PROTECTED REGION END -----*/	//	SYBPM_SparkAll::Additional Method prototypes
};

/*----- PROTECTED REGION ID(SYBPM_SparkAll::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions

/*----- PROTECTED REGION END -----*/	//	SYBPM_SparkAll::Additional Classes Definitions

}	//	End of namespace

#endif   //	SYBPM_SparkAll_H
