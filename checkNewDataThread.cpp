static const char *RcsId = "$Id:  $";
//+=============================================================================
//
// file :         checkNewDataThread.cpp
//
// description :  C++ source for the LiberaSparkAll 
//
// project :      LiberaSparkAll 
//
// $Author:  $
//
// $Revision:  $
//
// $Revision:  $
// $Date:  $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source:  $
// $Log:  $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================

#include <SYBPM_SparkAll.h>
#include <checkNewDataThread.h>

namespace SYBPM_SparkAll_ns
{

//+----------------------------------------------------------------------------
//
// method :         __checkNewDataThread::_checkNewDataThread()
//
// description :	Starts the simulation thread and undetach it
//
//-----------------------------------------------------------------------------
_checkNewDataThread::_checkNewDataThread(SYBPM_SparkAll *_LiberaSparkAll, omni_mutex &shared_data_mutex) : Tango::LogAdapter(_LiberaSparkAll), omni_thread(),
		 ParamSurveySharedDataMutex(shared_data_mutex)
{
	string dummy = RcsId;       // To avoid gcc unused variable warning
	p_LiberaSparkAll = _LiberaSparkAll;
  tickStart = -1;
  lastReadTime = 0;
  lastReadDate = 0;
  lastReadPeriod = 0;

	cout << "Undetach checkNewDataThread\n";
    start_undetached();
}


//+----------------------------------------------------------------------------
//
// method :         __checkNewDataThread::run_undetached()
//
// description :	The period thread which simulates frames data at the same rate as
//					the real acquisition using DMA
//
//-----------------------------------------------------------------------------
void *_checkNewDataThread::run_undetached(void*)
{
	cout << "start checkNewDataThread run_undetached\n";

  long debugFlags;
  bool sendEventOnFillCounterChange;
  unsigned long	usleep_time = 5000; // 5ms polling for FillCounter check
  Tango::DevULong PreviousFillCounter;
  Tango::DevULong *p_status;
  Tango::DevULong min_value, max_value;
  Tango::DevULong *p_SparkFillCounter_buffer = new Tango::DevULong[NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL];

	sleep(1);

	// Reset the Fill counters
	//-------------------------------------------------------------------------------------------------
	p_LiberaSparkAll -> sendCommand("ResetFillCounter");

	{ _PSDATA_OMNI_MUTEX_;
		p_LiberaSparkAll -> ParamSurveySharedData.checkNewDataThreadStarted = true;
		p_LiberaSparkAll -> ParamSurveySharedData.FillCounter = 0; 
		p_LiberaSparkAll -> ParamSurveySharedData.PreviousFillCounter = 0;
    p_LiberaSparkAll -> ParamSurveySharedData.sparkTriggerMissed = false;
    sendEventOnFillCounterChange = p_LiberaSparkAll -> ParamSurveySharedData.sendEventOnFillCounterChange;
	} _PSDATA_OMNI_MUTEX_REL_;

	// Main loop
	//--------------------------------------------------------------------------------------
	sleep(1);

	for (long long loop=1;   ; loop++) {

		bool must_exit = false;
		{ _PSDATA_OMNI_MUTEX_;
			must_exit = p_LiberaSparkAll -> ParamSurveySharedData.thread_suicide;
			debugFlags = p_LiberaSparkAll -> ParamSurveySharedData.debugFlags;
			PreviousFillCounter = p_LiberaSparkAll -> ParamSurveySharedData.PreviousFillCounter;
		} _PSDATA_OMNI_MUTEX_REL_;

		// If the thread should exit
		//-----------------------------------------------------------------------------
		if (must_exit == true) {
			delete p_SparkFillCounter_buffer; 
			cout << "checkNewDataThread exiting\n";
			return NULL;
		}

		// Checks FillCounter
		//--------------------------------------------------------------------------------------
		{ _PSDATA_OMNI_MUTEX_;

			// Set buffer to 0
			Tango::DevULong *p_buffer = p_LiberaSparkAll -> ParamSurveySharedData.p_FillCounter_buffer;
			memset(p_buffer,0,TOTAL_BPM*sizeof(Tango::DevULong));

			// Read Spark device server FillCounter
			try {
				p_LiberaSparkAll -> readAttribute(	"FillCounter", 
												p_LiberaSparkAll -> ParamSurveySharedData.p_FillCounter_buffer,
												(Tango::DevULong) UINT32_MAX
											  );
			} catch (...) {
				cout << "Error reading FillCounter" << endl;
				continue;
			}

			// Compare if the values are the same
			//--------------------------------------------------------------------------------------
			p_buffer = p_LiberaSparkAll -> ParamSurveySharedData.p_FillCounter_buffer;
			p_status = &p_LiberaSparkAll -> ParamSurveySharedData.Status[0][0];
			max_value = 0;
			min_value = UINT32_MAX;

			for (size_t i=0; i < TOTAL_BPM; i++) {

				// Skip the NOT_DEFINED and DISABLE devices
				if ( (*p_status & (1 << LIBERA_NOT_DEFINED)) || (*p_status & (1 << DEVICE_DISABLE)) || (*p_status & (1 << LIBERA_SERVER_NOT_REACHABLE)) || (*p_status & (1 << LIBERA_IN_FAULT)) ) {
						p_buffer++;
						p_status++;
						continue;
				}

        // Skip read failure
				if( *p_buffer==UINT32_MAX ) {
          p_buffer++;
          p_status++;
          continue;
				}

				if (min_value > *p_buffer) min_value = *p_buffer;
				if (max_value < *p_buffer) max_value = *p_buffer;

				p_buffer++;
				p_status++;
			}

		} _PSDATA_OMNI_MUTEX_REL_;

		if (min_value == max_value) {
			if (min_value == PreviousFillCounter ) {
        // Values are equal but the FillCounters have not changed  --> No new trigger, no new data
				usleep(usleep_time);
				continue;
			}
		}

		if( (max_value-min_value)>1 ) {

      // A spark is blocked or has a non coherent fill counter or has missed a trigger
      { _PSDATA_OMNI_MUTEX_;
        p_LiberaSparkAll -> ParamSurveySharedData.PreviousFillCounter = max_value;
        p_LiberaSparkAll -> ParamSurveySharedData.FillCounter = max_value;
        p_LiberaSparkAll -> ParamSurveySharedData.FillCounter_Coherent = false;
        if( !p_LiberaSparkAll -> ParamSurveySharedData.sparkTriggerMissed ) {
          cout << " Spark trigger failed (" << min_value << "," << max_value << ")!" << endl;
          p_LiberaSparkAll -> ParamSurveySharedData.sparkTriggerMissed = true;
          p_LiberaSparkAll -> addError("A spark has missed a trigger, please reset counter");
        }
      } _PSDATA_OMNI_MUTEX_REL_;

		} else {

      if (max_value==min_value) {

        if (min_value == (PreviousFillCounter + 1)) {

          // All buffers have been filled, we can read data
          time_t now = get_ticks();
          lastReadPeriod = now - lastReadDate;
          lastReadDate = now;
          read_data(debugFlags);
          lastReadTime = get_ticks() - now;

          // Checks if the FillCounters have not been incremented during reading. That means that data might be corrupted
          bool ok = checkFillCountersAgain(debugFlags, p_LiberaSparkAll->ParamSurveySharedData.p_FillCounter_buffer);

          // Push data ready event
          if (sendEventOnFillCounterChange && ok)
            p_LiberaSparkAll->push_data_ready_event("FillCounter", (Tango::DevLong) max_value);

          {
            _PSDATA_OMNI_MUTEX_;
            p_LiberaSparkAll->ParamSurveySharedData.PreviousFillCounter = max_value;
            p_LiberaSparkAll->ParamSurveySharedData.FillCounter = max_value;
            p_LiberaSparkAll->ParamSurveySharedData.FillCounter_Coherent = true;
          }
          _PSDATA_OMNI_MUTEX_REL_;

        } else {

          // We missed a trigger
          {
            _PSDATA_OMNI_MUTEX_;
            p_LiberaSparkAll->ParamSurveySharedData.PreviousFillCounter = max_value;
            p_LiberaSparkAll->ParamSurveySharedData.FillCounter = max_value;
            p_LiberaSparkAll->ParamSurveySharedData.FillCounter_Coherent = false;
          }
          _PSDATA_OMNI_MUTEX_REL_;
          cout << " Trigger missed (" << PreviousFillCounter << "," << min_value << "," << max_value << ")!" << endl;

        }

      }

		}



		usleep(usleep_time);
	}
}



//--------------------------------------------------------
/**
 *	Method      : read_data
 *	Description : read the data according to set flags
 */
//--------------------------------------------------------
void _checkNewDataThread::read_data(long debugFlags)
{
		bool        QSUM_Enable;
		bool        AMPL_Enable;
		Tango::DevULong QSUM_Threshold;

		{ _PSDATA_OMNI_MUTEX_;
			QSUM_Enable   			= p_LiberaSparkAll -> ParamSurveySharedData.QSUM_Enable;
			QSUM_Threshold   		= p_LiberaSparkAll -> ParamSurveySharedData.QSUM_Threshold;
			AMPL_Enable             = p_LiberaSparkAll -> ParamSurveySharedData.AMPL_Enable;
		} _PSDATA_OMNI_MUTEX_REL_;

		// Read the state of all the configured Liberas Sparks
		// Read TBT_XPosition_Mean
		//--------------------------------------------------------------------------------------
		{ _PSDATA_OMNI_MUTEX_;

			p_LiberaSparkAll -> readLiberaBPMStatus();

			Tango::DevDouble *p_buffer = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_XPosition_Mean_buffer;
			Tango::DevDouble *p_buffer_minusref = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_XPositionMinusRef_buffer;
			for (size_t i=0; i < (NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL); i++)	{
	  			*p_buffer++ = NaN;
	  			*p_buffer_minusref++ = NaN;
			}
			p_LiberaSparkAll -> readAttributeDouble("TBT_XPosition_Mean", p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_XPosition_Mean_buffer, 0.0);

			// Substract the reference
			//--------------------------------------------------------------------------------------
			p_buffer = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_XPosition_Mean_buffer;
			p_buffer_minusref = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_XPositionMinusRef_buffer;
			Tango::DevDouble *p_buffer_ref = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_XPositionRef_buffer;

			for (size_t i=0; i < (NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL); i++) *p_buffer_minusref++ = (*p_buffer++) - (*p_buffer_ref++); 

		} _PSDATA_OMNI_MUTEX_REL_;
		usleep(50);  //  Sleep for 50 uS

		// Read TBT_ZPosition_Mean
		//--------------------------------------------------------------------------------------
		{ _PSDATA_OMNI_MUTEX_;

			Tango::DevDouble *p_buffer = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_ZPosition_Mean_buffer;
			Tango::DevDouble *p_buffer_minusref = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_ZPositionMinusRef_buffer;
			for (size_t i=0; i < (NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL); i++)	{
				*p_buffer++ = NaN;
				*p_buffer_minusref++ = NaN;
			}

			p_LiberaSparkAll -> readAttributeDouble("TBT_ZPosition_Mean", p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_ZPosition_Mean_buffer, 0.0);

			// Substract the reference
			//--------------------------------------------------------------------------------------
			p_buffer = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_ZPosition_Mean_buffer;
			p_buffer_minusref = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_ZPositionMinusRef_buffer;
			Tango::DevDouble *p_buffer_ref = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_ZPositionRef_buffer;

			for (size_t i=0; i < (NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL); i++) *p_buffer_minusref++ = (*p_buffer++) - (*p_buffer_ref++); 

		} _PSDATA_OMNI_MUTEX_REL_;
		usleep(50);  //  Sleep for 50 uS


		// Read always the TBT_Sum_Mean 
		//--------------------------------------------------------------------------------------
		{ _PSDATA_OMNI_MUTEX_;

			Tango::DevDouble *p_buffer = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Sum_Mean_buffer;
			Tango::DevDouble *p_buffer_minusref = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_SumMinusRef_buffer;
			for (size_t i=0; i < (NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL); i++) {
				*p_buffer++ = NaN;
				*p_buffer_minusref++ = NaN;
			}

			p_LiberaSparkAll -> readAttributeDouble("TBT_Sum_Mean", p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Sum_Mean_buffer, 0.0);

			// Substract the reference
			//--------------------------------------------------------------------------------------
			p_buffer = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Sum_Mean_buffer;
			p_buffer_minusref = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_SumMinusRef_buffer;
			Tango::DevDouble *p_buffer_ref = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_SumRef_buffer;

			for (size_t i=0; i < (NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL); i++) {
				if (*p_buffer_ref != 0.0) *p_buffer_minusref = (100 * (*p_buffer - *p_buffer_ref)) / *p_buffer_ref; 
				p_buffer++;
				p_buffer_ref++;
				p_buffer_minusref++;
			}

		} _PSDATA_OMNI_MUTEX_REL_;
		usleep(50);  //  Sleep for 50 uS

		// If QSUM_Enable is enable, put NaN in the X and Z positions if the sum is below the threshold
		//--------------------------------------------------------------------------------------
		{ _PSDATA_OMNI_MUTEX_;

			if (QSUM_Enable) {
				Tango::DevDouble *p_buffer_sum  = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Sum_Mean_buffer;
				Tango::DevDouble *p_buffer_Xpos = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_XPosition_Mean_buffer;
				Tango::DevDouble *p_buffer_Zpos = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_ZPosition_Mean_buffer;
				for (size_t i=0; i < (NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL); i++) {
					if (*p_buffer_sum < QSUM_Threshold) {
						*p_buffer_Xpos = NaN;	
						*p_buffer_Zpos = NaN;	
					};
					p_buffer_sum++;
					p_buffer_Xpos++;	
					p_buffer_Zpos++;	
				}
			}

		} _PSDATA_OMNI_MUTEX_REL_;
		usleep(50);  //  Sleep for 50 uS

		// Read TBT_Va_Mean, Vb_Mean, Vc_Mean, Vd_Mean
		//--------------------------------------------------------------------------------------
		{ _PSDATA_OMNI_MUTEX_;

			// Set buffer to NaN
			Tango::DevDouble *p_buffer_Va = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Va_Mean_buffer;
			Tango::DevDouble *p_buffer_Vb = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Vb_Mean_buffer;
			Tango::DevDouble *p_buffer_Vc = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Vc_Mean_buffer;
			Tango::DevDouble *p_buffer_Vd = p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Vd_Mean_buffer;
			for (size_t i=0; i < (NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL); i++) 	{
				*p_buffer_Va++ = NaN;
				*p_buffer_Vb++ = NaN;
				*p_buffer_Vc++ = NaN;
				*p_buffer_Vd++ = NaN;
			}

			if (AMPL_Enable)  {
				p_LiberaSparkAll -> readAttributeDouble("TBT_Va_Mean", p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Va_Mean_buffer, 0.0);
				usleep(50);  //  Sleep for 50 uS
				p_LiberaSparkAll -> readAttributeDouble("TBT_Vb_Mean", p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Vb_Mean_buffer, 0.0);
				usleep(50);  //  Sleep for 50 uS
				p_LiberaSparkAll -> readAttributeDouble("TBT_Vc_Mean", p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Vc_Mean_buffer, 0.0);
				usleep(50);  //  Sleep for 50 uS
				p_LiberaSparkAll -> readAttributeDouble("TBT_Vd_Mean", p_LiberaSparkAll -> ParamSurveySharedData.p_TBT_Vd_Mean_buffer, 0.0);
				usleep(50);  //  Sleep for 50 uS
			}

		} _PSDATA_OMNI_MUTEX_REL_;
		usleep(50);  //  Sleep for 50 uS

}

//--------------------------------------------------------
/**
 *	Method      : checkFillCountersAgain
 *	Description : Verify that a Fillcounter has not been incremented during data reading
 */
//--------------------------------------------------------
bool _checkNewDataThread::checkFillCountersAgain(long debugFlags, Tango::DevULong *p_FillCounter_buffer)
{

	// Read again all the FillCounter and compare to previous values
	//--------------------------------------------------------------------------------------
	Tango::DevULong *p_check_buffer = new Tango::DevULong[NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL]; 

	Tango::DevULong *pp_check_buffer = p_check_buffer; 
	for (size_t i=0; i < (NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL); i++)  *pp_check_buffer++ = 0;

	p_LiberaSparkAll -> readAttribute(	"FillCounter", 
										p_check_buffer,
										(Tango::DevULong) UINT32_MAX
									  );

	Tango::DevULong *p_new_values = p_check_buffer; 
	Tango::DevULong *p_old_values = p_FillCounter_buffer;

	bool equal = true;

	for (size_t i=0; i< (NUMBER_OF_CELLS * NUMBER_OF_BPM_PER_CELL) ; i++) {	
		if (*p_old_values++ != *p_new_values++) {
			equal = false;
		}
	}

	if ( ! equal) { 
		string l_error = "Data read too slow  ( reading=" + to_string(lastReadTime) + " ms, period=" +  to_string(lastReadPeriod) + "ms )";
		p_LiberaSparkAll -> addError(l_error);
		cout << l_error << endl;
	}

	delete p_check_buffer;
	return equal;

}

time_t _checkNewDataThread::get_ticks() {

  if(tickStart < 0 )
    tickStart = time(NULL);

  struct timeval tv;
  gettimeofday(&tv,NULL);
  return ( (tv.tv_sec-tickStart)*1000 + tv.tv_usec/1000 );

}


}
